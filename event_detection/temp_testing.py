# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
repetitions = 10


timeit_setup = '''
import numpy as np

X = np.random.random((3000,4))
r = np.random.random((1, 4))
def pairwise_numpy(in_vec, radii):
    return np.sqrt((((in_vec[:, None, :] - in_vec) / radii[None, None,:]) ** 2).sum(-1) )


'''

import timeit
t1 = timeit.Timer("pairwise_numpy(X, r)", setup = timeit_setup)
print t1.timeit(repetitions) / repetitions * 1000, 'ms'





timeit_setup = '''
import numpy as np
from scipy.spatial.distance import cdist

X = np.random.random((3000,4))
r = np.random.random((1, 4))
def pairwise_scipy(X,r):
    cdist(X, X, 'seuclidean', V=r.reshape((r.size,)) );

'''

import timeit
t1 = timeit.Timer("pairwise_scipy(X, r)", setup = timeit_setup)
print t1.timeit(repetitions) / repetitions * 1000, 'ms'


