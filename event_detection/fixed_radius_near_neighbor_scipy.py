# -*- coding: utf-8 -*-
"""
Created on Mon Jun 30 18:33:07 2014

@author: iss
"""

from scipy.spatial.distance import cdist

def pairwise_scipy(X,r):
    cdist(X, X, 'seuclidean', V=r.reshape((r.size,)) );